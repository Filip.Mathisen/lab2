package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() < 20) {
            items.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items.isEmpty()) {
            throw new NoSuchElementException();
        }
        items.remove(item);
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).hasExpired()) {
                expiredItems.add(items.get(i));
                items.remove(i);
                i -= 1;
            }
        }
        return expiredItems;
    }
}
